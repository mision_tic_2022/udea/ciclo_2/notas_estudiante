/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.universidad_gui.notas_estudiante.modelo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author darioandres
 */
public class EstudianteTest {
    
    Estudiante testEstudiante;
    
    public EstudianteTest() {
        testEstudiante = new Estudiante("Andres", "Quintero", 20);
    }
    
    

    /**
     * Test of getNombre method, of class Estudiante.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        try{
            //Estudiante instance = new Estudiante("Andres", "Quintero", 'A');
            //Resultado esperado
            String expResult = "Andres";
            //Resultado obtenido
            String result = testEstudiante.getNombre();
            assertEquals(expResult, result);
        }catch(Exception error){
          fail("Falló en la construcción del objeto");  
        }  
    }
    
    @Test
    public void testGetNotasCaso1(){
        System.out.println("GetNotasCaso1");
        double[] expResult = {0,0,0,0,0};
        assertArrayEquals(expResult, testEstudiante.getNotas());
    }
    
    @Test
    public void testGetNotasCaso2(){
        System.out.println("GetNotasCaso2");
        double[] expResult = {3.5,4.2,5.0,3.1,2.5};
        testEstudiante.setNotas(expResult);
        assertArrayEquals(expResult, testEstudiante.getNotas());
    }
    
    @Test
    public void testGetNotasCaso3(){
        System.out.println("GetNotasCaso3");
        double[] expResult = {4.2,4.2,4.2,4.2,5.0};
        
        for(int i = 0; i < testEstudiante.getNotas().length; i++){
            if(i == testEstudiante.getNotas().length-1){
                testEstudiante.setNota(5.0, i);
            }else{
                testEstudiante.setNota(4.2, i);
            }
        }
        
        assertArrayEquals(expResult, testEstudiante.getNotas());
    }
    
    @Test
    public void testCalcularPromedioNotasCaso1(){
        System.out.println("CalcularPromedioCaso1");
        double promedioEsperado = 4.36;
        assertEquals(promedioEsperado, testEstudiante.calcularPromedioNotas());
        
    }
    
    @Test
    public void testCalcularPromedioNotasCaso2(){
        System.out.println("CalcularPromedioCaso2");
        double promedioEsperado = 4.0;
        assertNotEquals(promedioEsperado, testEstudiante.calcularPromedioNotas());
        
    }
    
    
}
